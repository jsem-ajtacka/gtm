# GTM extension for Nette Framework

This is a simple Nette Framework extension for GTM.
The GTM code is shown only in production mode to avoid tracking during development.

## Installation

Use Composer to install this extension:

```
$ composer reguire jsemajtacka/gtm
```

## Configuration

Add this to your configuration .neon file:

```
extensions:
    gtm: JsemAjtacka\GTM\DI\GtmExtension

gtm:
    id: 'yourGtmId'
```

## Example of usage

`BasePresenter.php:`

```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject]
    public GtmFactory $gtmFactory;

    protected function createComponentGtm(): GtmControl
    {
        return $this->gtmFactory->create();
    }
}
```

`@layout.latte:`
```latte
{control gtm}
```