<?php

namespace JsemAjtacka\GTM\DI;

use JsemAjtacka\GTM\GtmControl;
use JsemAjtacka\GTM\GtmFactory;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

final class GtmExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'id' => Expect::string()
        ]);
    }

    public function loadConfiguration(): void
    {
        $id = $this->config->id;
        $productionMode = $this->getContainerBuilder()->parameters['productionMode'];

        $this->getContainerBuilder()
            ->addDefinition($this->prefix('gtm'))
            ->setFactory(GtmFactory::class)
            ->setArgument('id', $id)
            ->setArgument('productionMode', $productionMode);
    }
}