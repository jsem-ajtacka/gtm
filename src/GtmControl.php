<?php

namespace JsemAjtacka\GTM;

use Nette\Application\UI\Control;

class GtmControl extends Control
{
    public function __construct(private string $id, private bool $productionMode) {}

    public function render(): void
    {
        $this->template->id = $this->id;
        $this->template->productionMode = $this->productionMode;
        $this->template->setFile(__DIR__ . '/templates/gtm.latte');
        $this->template->render();
    }
}