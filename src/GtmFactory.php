<?php

namespace JsemAjtacka\GTM;

class GtmFactory
{
    public function __construct(private string $id, private bool $productionMode) {}

    public function create(): GtmControl {
        return new GtmControl($this->id, $this->productionMode);
    }
}